package com.jimmytai.demo.whizcarpet;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.jimmytai.whizcarpet.WhizCarpetView;

import org.json.JSONArray;
import org.json.JSONException;

public class MainActivity extends Activity {

    private WhizCarpetView carpetView;
    private Button btn_changeLayout;
    int count = 0;
    int countSensor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        carpetView = (WhizCarpetView) findViewById(R.id.carpet);
        btn_changeLayout = (Button) findViewById(R.id.btn_changeLayout);
        btn_changeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String j;
                if (count % 2 == 0) {
                    j = "[[1,1,1,1,1],[1,0,0,0,0],[1,0,0,0,0]]";
                } else {
                    j = "[[1,1,1]]";
                }
                try {
                    JSONArray array = new JSONArray(j);
                    carpetView.setLayout(array);
                } catch (JSONException | WhizCarpetView.LayoutFormatException e) {
                    e.printStackTrace();
                }
                count++;
            }
        });

        findViewById(R.id.btn_updateSensor).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s;
                if (countSensor % 2 == 0) {
                    s = "[{\"row\":0,\"col\":0,\"s0\":0,\"s1\":1,\"s2\":8,\"s3\":0}]";
                } else {
                    s = "[{\"row\":0,\"col\":0,\"s0\":0,\"s1\":0,\"s2\":0,\"s3\":0}]";
                }
                try {
                    JSONArray array = new JSONArray(s);
                    carpetView.updateSensor(array);
                } catch (WhizCarpetView.SensorFormatException | JSONException e) {
                    e.printStackTrace();
                }
                countSensor++;
            }
        });

        try {
            String j = "[[1,1,1]]";
            JSONArray array = new JSONArray(j);
            carpetView.setLayout(array);
        } catch (JSONException | WhizCarpetView.LayoutFormatException e) {
            e.printStackTrace();
        }
    }
}
