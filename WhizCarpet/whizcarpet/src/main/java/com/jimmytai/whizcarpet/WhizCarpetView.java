package com.jimmytai.whizcarpet;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by GRC on 2016/8/24.
 */
public class WhizCarpetView extends View {

    private static final String TAG = "WhizCarpetView";

    private static final int DEFAULT_SIZE = 250;
    private static final int DEFAULT_CARPET_PREVIEW_ROW = 1;
    private static final int DEFAULT_CARPET_PREVIEW_COL = 1;
    private static final float DEFAULT_CARPET_RADIUS = 10;
    private static final float DEFAULT_CARPET_FRAME_SCALE = 2f;
    private static final float DEFAULT_SENSOR_GAP_SCALE = 1f;
    private static final int DEFAULT_CARPET_COLOR = Color.argb(0xFF, 0xC5, 0xC9, 0xe1);
    private static final int DEFAULT_CARPET_PRESSED_COLOR = Color.argb(0xFF, 0x88, 0x96, 0xE7);
    private static final int DEFAULT_CARPET_FRAME_COLOR = Color.argb(0xFF, 0xFD, 0xDA, 0xCF);

    private Context context;

    private int viewWidth, viewHeight;
    private float carpetFrameScale, carpetGapScale, carpetRadius;
    private int carpetColor, carpetPressedColor, carpetFrameColor, carpetPreviewRow, carpetPreviewCol;
    private Paint transparentPaint, carpetPaint, carpetPressedPaint, carpetFramePaint;
    private RectF roundRec;
    private WhizCarpetView.Status[][] previewLayoutArray;

    public WhizCarpetView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public WhizCarpetView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init(attrs);
    }

    public WhizCarpetView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init(attrs);
    }

    private void init() {
        this.carpetPreviewRow = DEFAULT_CARPET_PREVIEW_ROW;
        this.carpetPreviewCol = DEFAULT_CARPET_PREVIEW_COL;
        this.carpetRadius = DEFAULT_CARPET_RADIUS;
        this.carpetFrameScale = DEFAULT_CARPET_FRAME_SCALE;
        this.carpetGapScale = DEFAULT_SENSOR_GAP_SCALE;
        this.carpetColor = DEFAULT_CARPET_COLOR;
        this.carpetPressedColor = DEFAULT_CARPET_PRESSED_COLOR;
        this.carpetFrameColor = DEFAULT_CARPET_FRAME_COLOR;
        createPaint();
        createPreview();
    }

    private void init(AttributeSet attrs) {
        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.WhizCarpetView, 0, 0);
        try {
            this.carpetPreviewRow = a.getInt(R.styleable.WhizCarpetView_carpetPreviewRow, DEFAULT_CARPET_PREVIEW_ROW);
            this.carpetPreviewCol = a.getInt(R.styleable.WhizCarpetView_carpetPreviewCol, DEFAULT_CARPET_PREVIEW_COL);
            this.carpetRadius = a.getDimension(R.styleable.WhizCarpetView_carpetRadius, DEFAULT_CARPET_RADIUS);
            this.carpetFrameScale = a.getFloat(R.styleable.WhizCarpetView_carpetFrameScale, DEFAULT_CARPET_FRAME_SCALE);
            this.carpetGapScale = a.getFloat(R.styleable.WhizCarpetView_sensorGapScale, DEFAULT_SENSOR_GAP_SCALE);
            this.carpetColor = a.getColor(R.styleable.WhizCarpetView_carpetColor, DEFAULT_CARPET_COLOR);
            this.carpetPressedColor = a.getColor(R.styleable.WhizCarpetView_carpetPressedColor, DEFAULT_CARPET_PRESSED_COLOR);
            this.carpetFrameColor = a.getColor(R.styleable.WhizCarpetView_carpetFrameColor, DEFAULT_CARPET_FRAME_COLOR);
        } finally {
            a.recycle();
        }
        createPaint();
        createPreview();
    }

    private void createPaint() {
        roundRec = new RectF();

        transparentPaint = new Paint();
        transparentPaint.setAntiAlias(true);
        transparentPaint.setColor(Color.TRANSPARENT);
        transparentPaint.setStyle(Paint.Style.FILL);

        carpetPaint = new Paint();
        carpetPaint.setAntiAlias(true);
        carpetPaint.setColor(carpetColor);
        carpetPaint.setStyle(Paint.Style.FILL);

        carpetPressedPaint = new Paint();
        carpetPressedPaint.setAntiAlias(true);
        carpetPressedPaint.setColor(carpetPressedColor);
        carpetPressedPaint.setStyle(Paint.Style.FILL);

        carpetFramePaint = new Paint();
        carpetFramePaint.setAntiAlias(true);
        carpetFramePaint.setColor(carpetFrameColor);
        carpetFramePaint.setStyle(Paint.Style.STROKE);
    }

    private void createPreview() {
        previewLayoutArray = new WhizCarpetView.Status[carpetPreviewCol * 2][carpetPreviewRow * 2];
        for (int i = 0; i < carpetPreviewRow * 2; i++) {
            for (int j = 0; j < carpetPreviewCol * 2; j++) {
                previewLayoutArray[j][i] = WhizCarpetView.Status.NORMAL;
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.viewWidth = measureDimension((int) dip2px(context, DEFAULT_SIZE), widthMeasureSpec);
        this.viewHeight = measureDimension((int) dip2px(context, DEFAULT_SIZE), heightMeasureSpec);

        setMeasuredDimension(this.viewWidth, this.viewHeight);
    }

    protected int measureDimension(int defaultSize, int measureSpec) {
        int result;

        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // 此View尺寸給一定值或設為match parent && 父控件尺寸有一定值
            result = specSize;
        } else if (specMode == MeasureSpec.AT_MOST) {
            // 1. 此View設為wrap content 或設為 match parent && 父控件有尺寸限制
            result = Math.min(defaultSize, specSize);
        } else {
            // 此View尺寸不為定值 && 父控件沒有限制
            result = defaultSize;
        }
        return result;
    }

    private float dip2px(Context context, float dipValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return dipValue * scale + 0.5f;
    }

    private int carpetRow, carpetCol, sensorRow, sensorCol;
    private Status[][] layoutArray;

    public enum Status {
        EMPTY(-1), NORMAL(0), P1(1), P2(2), P3(3), P4(4), P5(5), P6(6), P7(7), P8(8), P9(9);

        private int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float gapSize = 15 * carpetGapScale, frameSize = 25 * carpetFrameScale, radius = carpetRadius;
        int row, col;
        float startX = 0, startY = 0;
        float sensorSize, carpetSize;

        WhizCarpetView.Status[][] la;

        if (layoutArray == null) {
            la = previewLayoutArray;
        } else {
            la = layoutArray;
        }

        try {
            col = la.length;
            row = la[0].length;

            if (viewWidth / col > viewHeight / row) {
                gapSize = gapSize / row;
                frameSize = frameSize / row;
                carpetSize = (float) viewHeight / (float) row * 2f - 2 * frameSize;
                startX = (viewWidth - carpetSize * (col / 2f) - frameSize * (col / 2f - 1)) / 2f;
                startY = (viewHeight - carpetSize * (row / 2f) - frameSize * (row / 2f - 1)) / 2f;
                sensorSize = (carpetSize - gapSize) / 2f;
            } else {
                gapSize = gapSize / col;
                frameSize = frameSize / col;
                carpetSize = (float) viewWidth / (float) col * 2f - 2 * frameSize;
                startY = (viewHeight - carpetSize * (row / 2f) - frameSize * (row / 2f - 1)) / 2f;
                startX = (viewWidth - carpetSize * (col / 2f) - frameSize * (col / 2f - 1)) / 2f;
                Log.d(TAG, "startX = " + startX);
                sensorSize = (carpetSize - gapSize) / 2f;
            }

            carpetFramePaint.setStrokeWidth(frameSize);

            float nowCarpetX, nowCarpetY = startY;
            float nowSensorX, nowSensorY = startY;
            float carpetOffset = 1;

            for (int y = 0; y < row / 2; y++) {
                nowCarpetX = startX;
                for (int x = 0; x < col / 2; x++) {
                    if (la[x * 2][y * 2] == Status.EMPTY) {
                        Log.d(TAG, "不畫巧拼 ==> X: " + x + "; Y: " + y + "; X位置: " + nowCarpetX + "; " +
                                "Y位置: " + nowCarpetY);
                    } else {
                        Log.d(TAG, "畫巧拼 ==> X: " + x + "; Y: " + y + "; X位置: " + nowCarpetX + "; " +
                                "Y位置: " + nowCarpetY);
                        roundRec.set(nowCarpetX, nowCarpetY, nowCarpetX + carpetSize,
                                nowCarpetY + carpetSize);
                        canvas.drawRoundRect(roundRec, radius, radius, carpetFramePaint);
                    }
                    nowCarpetX = nowCarpetX + carpetSize + frameSize;
                }
                nowCarpetY = nowCarpetY + carpetSize + frameSize;
            }

            for (int y = 0; y < row; y++) {
                nowSensorX = startX;
                for (int x = 0; x < col; x++) {

                    if (la[x][y] == Status.EMPTY) {
                        Log.i(TAG, "不畫感測器 ==> X: " + x + "; Y: " + y + "; X位置: " + nowSensorX +
                                "; " +
                                "Y位置: " + nowSensorY);
                    } else if (la[x][y] == Status.NORMAL) {
                        Log.i(TAG, "畫無壓力感測器 ==> X: " + x + "; Y: " + y + "; X位置: " + nowSensorX +
                                "; Y位置: " + nowSensorY);
                        if (x % 2 == 1 && y % 2 == 1) {
                            roundRec.set(nowSensorX + carpetOffset, nowSensorY + carpetOffset,
                                    nowSensorX + sensorSize, nowSensorY + sensorSize);
                        } else if (x % 2 == 0 && y % 2 == 1) {
                            roundRec.set(nowSensorX, nowSensorY, nowSensorX + sensorSize -
                                    carpetOffset, nowSensorY + sensorSize - carpetOffset);
                        } else if (x % 2 == 1 && y % 2 == 0) {
                            roundRec.set(nowSensorX + carpetOffset, nowSensorY,
                                    nowSensorX + sensorSize, nowSensorY + sensorSize - carpetOffset);
                        } else {
                            roundRec.set(nowSensorX, nowSensorY, nowSensorX + sensorSize -
                                    carpetOffset, nowSensorY + sensorSize - carpetOffset);
                        }
                        canvas.drawRoundRect(roundRec, radius, radius, carpetPaint);
                    } else {
                        Log.i(TAG, "畫有壓力感測器 ==> X: " + x + "; Y: " + y + "; X位置: " + nowSensorX +
                                "; Y位置: " + nowSensorY);
                        if (x % 2 == 1 && y % 2 == 1) {
                            roundRec.set(nowSensorX + carpetOffset, nowSensorY + carpetOffset,
                                    nowSensorX + sensorSize, nowSensorY + sensorSize);
                        } else if (x % 2 == 0 && y % 2 == 1) {
                            roundRec.set(nowSensorX, nowSensorY, nowSensorX + sensorSize -
                                    carpetOffset, nowSensorY + sensorSize - carpetOffset);
                        } else if (x % 2 == 1 && y % 2 == 0) {
                            roundRec.set(nowSensorX + carpetOffset, nowSensorY,
                                    nowSensorX + sensorSize, nowSensorY + sensorSize - carpetOffset);
                        } else {
                            roundRec.set(nowSensorX, nowSensorY, nowSensorX + sensorSize -
                                    carpetOffset, nowSensorY + sensorSize - carpetOffset);
                        }
                        canvas.drawRoundRect(roundRec, radius, radius, carpetPressedPaint);
                    }

                    if (x % 2 == 1)
                        nowSensorX = nowSensorX + sensorSize + frameSize;
                    else
                        nowSensorX = nowSensorX + sensorSize + gapSize;
                }
                if (y % 2 == 1)
                    nowSensorY = nowSensorY + sensorSize + frameSize;
                else
                    nowSensorY = nowSensorY + sensorSize + gapSize;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setLayout(JSONArray array) throws LayoutFormatException {
        if (array == null) {
            throw new LayoutFormatException("JsonArray is null");
        }
        if (array.length() == 0) {
            throw new LayoutFormatException("This layout does not contain any carpet");
        }
        carpetRow = array.length();
        try {
            for (int i = 0; i < array.length(); i++) {
                if (i != 0 && carpetCol != array.getJSONArray(i).length()) {
                    throw new LayoutFormatException("Amount of column is different");
                } else if (i == 0) {
                    carpetCol = array.getJSONArray(i).length();
                }
            }
            sensorRow = carpetRow * 2;
            sensorCol = carpetCol * 2;
            layoutArray = new Status[sensorCol][sensorRow];
            for (int y = 0; y < carpetRow; y++) {
                for (int x = 0; x < carpetCol; x++) {
                    Status value = array.getJSONArray(y).getInt(x) > 0 ? Status.NORMAL : Status.EMPTY;
                    layoutArray[x * 2][y * 2] = value;
                    layoutArray[x * 2 + 1][y * 2] = value;
                    layoutArray[x * 2][y * 2 + 1] = value;
                    layoutArray[x * 2 + 1][y * 2 + 1] = value;
                }
            }
            postInvalidate();
        } catch (JSONException e) {
            e.printStackTrace();
            throw new LayoutFormatException("Json format is not correct");
        }
    }

    public void updateSensor(JSONArray array) throws SensorFormatException {
        try {
            for (int i = 0; i < array.length(); i++) {
                JSONObject object = array.getJSONObject(i);
                int row = object.getInt("row"), col = object.getInt("col"), s0 = object.getInt("s0"), s1 = object.getInt("s1"), s2 =
                        object.getInt("s2"), s3 = object.getInt("s3");
                if (row >= carpetRow || col >= carpetCol) {
                    throw new SensorFormatException("Carpet is out of range");
                }
                layoutArray[col * 2][row * 2] = Status.values()[s0 + 1];
                layoutArray[col * 2][row * 2 + 1] = Status.values()[s1 + 1];
                layoutArray[col * 2 + 1][row * 2 + 1] = Status.values()[s2 + 1];
                layoutArray[col * 2 + 1][row * 2] = Status.values()[s3 + 1];
            }
            postInvalidate();
        } catch (JSONException e) {
            e.printStackTrace();
            throw new SensorFormatException("Sensor Json format is wrong");
        }
    }

    public class LayoutFormatException extends Exception {
        public LayoutFormatException(String message) {
            super(message);
        }
    }

    public class SensorFormatException extends Exception {
        public SensorFormatException(String message) {
            super(message);
        }
    }

    public void setCarpetFrameScale(float carpetFrameScale) {
        this.carpetFrameScale = carpetFrameScale;
    }

    public void setCarpetGapScale(float carpetGapScale) {
        this.carpetGapScale = carpetGapScale;
    }

    public void setCarpetRadius(float carpetRadius) {
        this.carpetRadius = carpetRadius;
    }

    public void setCarpetColor(int carpetColor) {
        this.carpetColor = carpetColor;
    }

    public void setCarpetPressedColor(int carpetPressedColor) {
        this.carpetPressedColor = carpetPressedColor;
    }

    public void setCarpetFrameColor(int carpetFrameColor) {
        this.carpetFrameColor = carpetFrameColor;
    }

    public float getCarpetFrameScale() {
        return carpetFrameScale;
    }

    public float getCarpetGapScale() {
        return carpetGapScale;
    }

    public float getCarpetRadius() {
        return carpetRadius;
    }

    public int getCarpetColor() {
        return carpetColor;
    }

    public int getCarpetPressedColor() {
        return carpetPressedColor;
    }

    public int getCarpetFrameColor() {
        return carpetFrameColor;
    }

    public int getCarpetRow() {
        return carpetRow;
    }

    public int getCarpetCol() {
        return carpetCol;
    }

    public int getSensorRow() {
        return sensorRow;
    }

    public int getSensorCol() {
        return sensorCol;
    }

    public Status[][] getLayoutArray() {
        return layoutArray;
    }
}
